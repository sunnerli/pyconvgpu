#include <stdio.h>
#include <cuda_runtime.h>
#include "image.c"

// macro to debug with CUDA
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }

/*
	Inline function to help debugging
*/
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

/*----------------------------------------------------------------------
 *					Variable	Declaration
 *----------------------------------------------------------------------*/
int *wPtr, *hPtr, *pPtr;
int *gpuR, *gpuG, *gpuB, *gpuK;
int *gpuResultR, *gpuResultG, *gpuResultB;
int convolutionLog = 0;

//-----------------------------------------------------------------------


/*
	Initial the GPU configuration

	Arg:	None
	Ret:	None
*/
extern "C" void initGPU(){
	int c;
	cudaGetDeviceCount(&c);
	if(c == 0){
		printf("There is no GPU on this device\n");
		return;
	}

	cudaSetDevice(1);
	printf("{CUDA}-->	Set GPU successful!\n");
}

/*
	GPU Convolution Function

	Arg:	img=> 	the input array that want to do convolution
			ker=> 	the kernel array to do convolution
			weight=>the weight of the input array
			height=>the height of the input array
			length=>the padding
			res=>	the convolution result
	Ret:	CUDA GPU function didn't have return value
*/
__global__ static void Convolution(int *img, int *ker, 
	int *weight, int *height, int *length, int *res){

	int convolutionLog = 0;
	
	//printf("weight: %d\theight: %d\tlength: %d\n", *weight, *height, *length);
	int m = *weight;	// the width of the image
	int n = *height; 	// the height of the image
	int p = *length;	// the length of the kernel

	// convolution
	int i, j, k, l;
	for(i=0; i<n-p+1; i++){
		for(j=0; j<m-p+1; j++){
			int _s = 0;
			for(k=0; k<p; k++){
				for(l=0; l<p; l++){
					_s = _s + ker[k*p+l] * img[i*m+j+k*m+l];
					if(convolutionLog == 1){
						printf("ker[%d]=%d\t, img[%d]=%d\t", 
							k*p+l, ker[k*p+l], i*m+j+k*m+l, img[i*m+j+k*m+l]);
						printf("_s: %d\n", _s);
					}
				}
				if(convolutionLog == 1)
					printf("\n");
			}
			res[i*m+j] = _s;
		}
	}

	// print result
	if(convolutionLog == 1){
		printf("result:\n");
		for(i=0; i<m-p+1; i++){
			for(j=0; j<n-p+1; j++)
				printf(" %d ", res[i*m+j]);
			printf("\n");
		}
	}
}

extern "C" void work(){
	// Create GPU memory
	cudaMalloc((void**) &gpuR, sizeof(int) * *cRedLength );
	cudaMalloc((void**) &gpuG, sizeof(int) * *cGreenLength );
	cudaMalloc((void**) &gpuB, sizeof(int) * *cBlueLength );
	cudaMalloc((void**) &gpuK, sizeof(int) * *cKernelLength );
	cudaMalloc((void**) &wPtr, sizeof(int) );
	cudaMalloc((void**) &hPtr, sizeof(int) );
	cudaMalloc((void**) &pPtr, sizeof(int) );
	cudaMalloc((void**) &gpuResultR, sizeof(int) * imageWeight * imageHeight);
	cudaMalloc((void**) &gpuResultG, sizeof(int) * imageWeight * imageHeight);
	cudaMalloc((void**) &gpuResultB, sizeof(int) * imageWeight * imageHeight);

	// Copy to GPU
	printf("{CUDA}-->	Copy image to GPU\n");
	cudaMemcpy(gpuR, cRed, sizeof(int) * *cRedLength, cudaMemcpyHostToDevice);
	cudaMemcpy(gpuG, cGreen, sizeof(int) * *cGreenLength, cudaMemcpyHostToDevice);
	cudaMemcpy(gpuB, cBlue, sizeof(int) * *cBlueLength, cudaMemcpyHostToDevice);
	cudaMemcpy(gpuK, cKernel, sizeof(int) * *cKernelLength, cudaMemcpyHostToDevice);
	cudaMemcpy(wPtr, &imageWeight, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(hPtr, &imageHeight, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(pPtr, &padding, sizeof(int), cudaMemcpyHostToDevice);


	// Convolution for r image	
	printf("{CUDA}-->	GPU convolution\n");
	Convolution<<<1, 1, 0>>>(gpuR, gpuK, wPtr, hPtr, pPtr, gpuResultR);
	// Convolution for g image
	Convolution<<<1, 1, 0>>>(gpuG, gpuK, wPtr, hPtr, pPtr, gpuResultG);
	// Convolution for b image
	Convolution<<<1, 1, 0>>>(gpuB, gpuK, wPtr, hPtr, pPtr, gpuResultB);
	cudaDeviceSynchronize();


	// Copy to host
	printf("{CUDA}-->	Copy result to host\n");
	resultR = (int*)malloc(sizeof(int) * imageWeight * imageHeight);
	resultG = (int*)malloc(sizeof(int) * imageWeight * imageHeight);
	resultB = (int*)malloc(sizeof(int) * imageWeight * imageHeight);

	cudaDeviceSynchronize();
	cudaMemcpy(resultR, gpuResultR, sizeof(int) * 16, cudaMemcpyDeviceToHost);
	cudaMemcpy( resultG, gpuResultG, sizeof(int) * *cGreenLength, cudaMemcpyDeviceToHost);
	cudaMemcpy(resultB, gpuResultB, sizeof(int) * *cBlueLength, cudaMemcpyDeviceToHost);


	// Free resource
	cudaFree(gpuR);
	cudaFree(gpuG);
	cudaFree(gpuB);
	cudaFree(gpuK);
	cudaFree(gpuResultR);
	cudaFree(gpuResultG);
	cudaFree(gpuResultB);
	printf("{CUDA}-->	CUDA progress end\n");
}

/*
int main(){
	initGPU();
	//GPULoad();
	return 0;
}
*/

