// Compile: gcc -shared -o conv.so -fPIC gpuCtl.c
#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include "image.c"


// the constant of the color index
const static int RED = 0;
const static int GREEN = 1;
const static int BLUE = 2;

/*
	Set the weight and the height of the image, including the padding

	Arg:	the weight, the height and the padding
	Ret:	None
*/
void assignWHP(int _w, int _h, int _p){
	imageWeight = _w;
	imageHeight = _h;
	padding = _p;
}

/*
	Set the length of the image for the particular pointer

	Arg:	the pointer that want to assign and the length number
	Ret:	None
*/
void _allocL(int **ptr, int **lptr, int _w){
	*ptr = (int*)malloc(sizeof(int) * _w);
	*lptr = (int*)malloc(sizeof(int));
	**lptr = _w;
}

/*
	Set the length of the image 

	Arg:	the color index and the length number
	Ret:	None
*/
int allocateLength(int color_index, int _length){
	switch(color_index){
		case 0:
			_allocL(&cRed, &cRedLength, _length);
			break;
		case 1:
			_allocL(&cGreen, &cGreenLength, _length);
			break;
		case 2:
			_allocL(&cBlue, &cBlueLength, _length);
			break;
		default:
			printf("wrong color index!\n");
	}
}

/*
	Set the length of the kernel 

	Arg:	the length number
	Ret:	None
*/
int allocateKernelLength(int _length){
	if(1){	// revise the pow conditon
		cKernel = (int*)malloc(sizeof(int) * _length);
		cKernelLength = (int*)malloc(sizeof(int));
		*cKernelLength = _length;
	}
}

/*
	Assign the value into the particular image pointer

	Arg:	the position and the color number
	Ret:	None
*/
void _setImgC(int *img, int position, int color){
	img[position] = color;
}

/*
	Assign the value into the image

	Arg:	the image index, the position and the color number
	Ret:	None
*/
void setImageColor(int color_index, int position, int color_number){
	//printf("color index: %d\tposition: %d\tcolor number: %d\n", color_index, position, color_number);
	switch(color_index){
		case 0:
			_setImgC(cRed, position, color_number);
			break;
		case 1:
			_setImgC(cGreen, position, color_number);
			break;
		case 2:
			_setImgC(cBlue, position, color_number);
			break;
		default:
			printf("wrong color index!\n");
	}
}

/*
	Assign the value into the kernel

	Arg:	the position and the kernel number
	Ret:	None
*/
void setKernelColor(int position, int color_number){
	if(cKernelLength[0] != 0 && position < *cKernelLength)
		cKernel[position] = color_number;		
}

/*
	Make GPU work

	Arg:	None
	Ret:	None
*/
void gpu_work(){
	work();
}

/*
	Initial the CUDA GPU configuration

	Arg:	None
	Ret:	None
*/
void gpu_init(){
	initGPU();
}

void seeResult(){
}

/*
	Get the color of result

	Arg:	the color index and the length number
	Ret:	None
*/
int getResult(int color_index, int position){
	if(position <= imageWeight * imageHeight){
		switch(color_index){
			case 0:		
				return resultR[position];
			case 1:
				return resultG[position];
			case 2:
				return resultB[position];
			default:
				printf("wrong color index!\n");
		}
	}
	printf("out of index.\n");
	return -1;
}

int main(){
}
