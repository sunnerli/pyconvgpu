# Define the compilier first
CPUCC=gcc
GPUCC=nvcc

# Include director
CUDALIB=-L/usr/local/cuda/lib64 -lcuda -lcudart -lcublas -lcurand
CUDAINCLUDE=-I/usr/local/cuda/include/
CUDAFLAG=-DGPU

# Declare the object file name
OBJS=conv

# Define the source c file name
SRC=conv

# Define the execute file name
EXEC=conv

# Define object file position
OBJDIR = ./

# Shared library flag
CPUFLAG= -shared -fPIC
GPUFLAG= -shared -Xcompiler -fPIC

all: $(EXEC)

$(EXEC): $(OBJS).o
	gcc $(CUDAFLAG) $(CUDAINCLUDE) $(CPUFLAG) $(SRC).c $^ -o $@.so $(CUDALIB)

$(OBJS).o: $(OBJS).cu
	nvcc $(GPUFLAG) $< -o $@

clean:
	rm -rf $(EXEC).so
	rm -rf $(EXEC).o