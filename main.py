from ctypes import *
#from conv import *
import os
import cv2
import numpy as np

"""
	The definition of the pyc class, it would do the convolution operation
"""
class pyc():
	# The information of image
	weight = -1
	height = -1
	klenth = -1

	# Image object
	rImg = None
	gImg = None
	bImg = None

	# Kernel object
	kernel = np.asarray([1, 2, 2, 1])

	def __init__(self):
		"""
			Constructor
		"""
		ppath = os.path.abspath(os.path.dirname(__file__))
		ppath = os.path.join(ppath, "conv.so")
		self.lib = cdll.LoadLibrary(ppath)
		self.loadFunction()

	def loadFunction(self):
		"""
			Load the c function
		"""
		# Load C function: gpu_init()
		self.gpu_init = self.lib.gpu_init
		self.gpu_init.argtypes = None
		self.gpu_init.restype= None

		# Load C function: allocateLength()
		self.allocateLength = self.lib.allocateLength
		self.allocateLength.argtypes = [c_int, c_int]
		self.allocateLength.restype= None

		# Load C function: setImageColor()
		self.setImageColor = self.lib.setImageColor
		self.setImageColor.argtypes = [c_int, c_int, c_int]
		self.setImageColor.restype= None

		# Load C function: gpu_work()
		self.gpu_work = self.lib.gpu_work
		self.gpu_work.argtypes = None
		self.gpu_work.restype= None

		# Load C function: allocateKernelLength()
		self.allocateKernelLength = self.lib.allocateKernelLength
		self.allocateKernelLength.argtypes = [c_int]
		self.allocateKernelLength.restype= None

		# Load C function: setKernelColor()
		self.setKernelColor = self.lib.setKernelColor
		self.setKernelColor.argtypes = [c_int, c_int]
		self.setKernelColor.restype= None

		# Load C function: assignWHP()
		self.assignWHP = self.lib.assignWHP
		self.assignWHP.argtypes = [c_int, c_int]
		self.assignWHP.restype= None

		# Load C function: seeResult()
		self.seeResult = self.lib.seeResult
		self.seeResult.argtypes = None
		self.seeResult.restype= None

		# Load C function: getResult()
		self.getResult = self.lib.getResult
		self.getResult.argtypes = [c_int, c_int]
		self.getResult.restype= c_int

	def assignHypher(self, w, h, p):
		"""
			(Deprecate Function)
			Assign the parameter of image and kernel

			Arg:	the weight, the height and the padding
			Ret:	None
		"""
		self.weight = w
		self.height = h
		self.padding = p
		self.assignWHP(w, h, p)	

	def assignPadding(self, p):
		"""
			Assign the kernel padding.
			Should assign the image parameter first

			Arg:	the padding
			Ret:	None
		"""
		if self.weight <= 0 or self.height <= 0:
			print "fatal error, you should assign the image first!"
		else:
			self.padding = p
			self.assignWHP(self.weight, self.height, p)

	def sendRed(self, r):
		"""
			Assign the red channel of whole image into C layer

			Arg:	the red image array
			Ret:	None
		"""
		self.allocateLength(0, len(r))
		for i in range(len(r)):
			self.setImageColor(0, i, r[i])

	def sendGreen(self, g):
		"""
			Assign the green channel of whole image into C layer

			Arg:	the green image array
			Ret:	None
		"""
		self.allocateLength(1, len(g))
		for i in range(len(g)):
			self.setImageColor(1, i, g[i])

	def sendBlue(self, b):
		"""
			Assign the blue channel of whole image into C layer

			Arg:	the blue image array
			Ret:	None
		"""
		self.allocateLength(2, len(b))
		for i in range(len(b)):
			self.setImageColor(2, i, b[i])

	def sendKernel(self, k):
		"""
			Assign the kernel into C layer

			Arg:	None
			Ret:	None
		"""
		#k = [1, 2, 2, 1]
		k = self.D3_D1(k)
		self.allocateKernelLength(len(k))
		for i in range(len(k)):
			self.setKernelColor(i, k[i])
		self.gpu_work()

	def seeResult(self):
		"""
			(Deprecated function)
			See the result after convolution

			Arg:	None
			Ret:	None
		"""
		self.seeResult();

	def gpu_init(self):
		"""
			Enable the CUDA GPU configuration

			Arg:	None
			Ret:	None
		"""
		self.gpu_init()

	def getRed(self):
		"""
			Get the red image result from C layer and print it

			Arg:	None
			Ret:	None
		"""
		res = []
		resAlt = []
		for i in range(self.weight * self.height):
			res.append(self.getResult(0, i))
		for i in range(self.height-self.padding+1):
			for j in range(self.weight-self.padding+1):
				resAlt.append(res[i*self.weight+j])
		return resAlt

	def getGreen(self):
		"""
			Get the green image result from C layer and print it

			Arg:	None
			Ret:	None
		"""
		res = []
		resAlt = []
		for i in range(self.weight * self.height):
			res.append(self.getResult(1, i))
		for i in range(self.height-self.padding+1):
			for j in range(self.weight-self.padding+1):
				resAlt.append(res[i*self.weight+j])
		return resAlt

	def getBlue(self):
		"""
			Get the blue image result from C layer and print it

			Arg:	None
			Ret:	None
		"""
		res = []
		resAlt = []
		for i in range(self.weight * self.height):
			res.append(self.getResult(2, i))
		for i in range(self.height-self.padding+1):
			for j in range(self.weight-self.padding+1):
				resAlt.append(res[i*self.weight+j])
		return resAlt



	def storeImg(self, img):
		"""
			Store the image information

			Arg:	image want to deal with
			Ret:	None
		"""
		# Store image parameter
		self.weight = img.shape[1]
		self.height = img.shape[0]

		# Split image
		self.split(img)
		self.rImg = self.D3_D1(self.rImg)
		self.gImg = self.D3_D1(self.gImg)
		self.bImg = self.D3_D1(self.bImg)

		#print "r: ", self.rImg
		#print "g: ", self.gImg
		#print "b: ", self.bImg

	def storeKernel(self, ker):
		"""
			Store the kernel information

			Arg:	the kernel numpy array
			Ret:	None
		"""
		self.kernel = ker
		self.klenth = ker.shape[0]

	def split(self, img):
		"""
			Split the image toward three channel(R, G, B)

			Arg:	the image want to deal with
			Ret:	None
		"""
		#print img
		self.rImg = self.D3_D1(img[:, :, 2:3])
		self.gImg = self.D3_D1(img[:, :, 1:2])
		self.bImg = self.D3_D1(img[:, :, :1])

	def D3_D1(self, img):
		"""
			reshape the one-channel image

			Arg:	the 3D one-channel image
			Ret:	the 1D one-channel image
		"""
		return img.flatten()


	def work(self):
		"""
			Python convoluton main function
			It would initialize the GPU first, 
			and send the image & kernel into C.
			Then it would do convolution.
			At last, it would get the result.

			Arg:	None
			Ret:	None
		"""
		self.gpu_init()
		self.sendRed(self.rImg)
		self.sendGreen(self.gImg)
		self.sendBlue(self.bImg)
		self.sendKernel(self.kernel)
		self.seeResult()
		self.rImg = self.getRed()
		self.gImg = self.getGreen()
		self.bImg = self.getBlue()

	def get(self):
		"""
			Get the convolution result after done
			Notice it should be called after convolution work

			Arg:	None
			Ret:	None
		"""
		relength = self.weight - self.padding + 1
		r_ = np.array((np.array((self.rImg)).reshape(relength, -1)))
		g_ = np.array((np.array((self.gImg)).reshape(relength, -1)))
		b_ = np.array((np.array((self.bImg)).reshape(relength, -1)))

		return np.dstack((r_, g_, b_))

"""
----------------------------------------------------------------
						Define Function
----------------------------------------------------------------
"""

def enlarge(img):
	"""
		Enlarge the image with 10000 times

		Arg:	image you want to enlarge
		Ret:	the enlarge image
	"""
	res = None
	for i in range(100):
		_ = img
		for j in range(100):
			_ = np.concatenate((_, img), axis=0)
		if type(res) == type(None):
			res = _
		else:
			res = np.concatenate((res, _), axis=1)
	return res

def bright(img):
	"""
		(Deprecated Function)
		Brighten the image

		Arg:	image you want to make it bright
		Ret:	the brighten image
	"""
	for i in range(3):
		for j in range(3):
			img[i][j] *= 10
	return img

"""
----------------------------------------------------------------
						Main
----------------------------------------------------------------
"""

# Declare the image, kernel and convolution object
img = np.asarray([[[1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1]], \
		[[2, 2, 2], [2, 2, 2], [2, 2, 2], [2, 2, 2]],\
		[[3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3]],\
		[[4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4]]])
ker = np.asarray([[1, 2], [2, 1]])
obj = pyc()

# Show image before convolution
print ""
print "Before convolution:"
print img
print ""

# Store the image and kernel into object
obj.storeImg(img)
obj.storeKernel(ker)
obj.assignPadding(2)

# Convolution!
obj.work()

# Show image before convolution
print ""
print "After convolution:"
print obj.get()
print ""

# Show result in opencv
cv2.imshow('show', enlarge(obj.get()))
cv2.waitKey()